import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';

const config = { /* Firebase config */
  apiKey: "AIzaSyCRtHHp2fOogaMwpNn-uHkoV9albR_xgJg",
  authDomain: "chat-web-3c416.firebaseapp.com",
  databaseURL: "https://chat-web-3c416.firebaseio.com",
  projectId: "chat-web-3c416",
  storageBucket: "chat-web-3c416.appspot.com",
  messagingSenderId: "101485590202",
  appId: "1:101485590202:web:48551abaf73fcd62857483",
  measurementId: "G-PNQGQ85CQ8"
}

firebase.initializeApp(config);
const provider = new firebase.auth.GoogleAuthProvider();
export const signInWithGoogle = () => {
  auth.signInWithPopup(provider);
};
export const auth = firebase.auth();
export const firestore = firebase.firestore()

export const generateUserDocument = async (user, additionalData) => {
  if (!user) return;
  const userRef = firestore.doc(`users/${user.uid}`);
  const snapshot = await userRef.get();
  if (!snapshot.exists) {
    const {email, displayName, photoURL } = user;
    try {
      const uniqRef = firestore.collection('users');
      const snap = await uniqRef.where('displayName', '==', additionalData.displayName).get();
      let _id = snap.size;
      let chats = [];

      await userRef.set({
        _id,
        displayName,
        email,
        photoURL,
        chats,
        ...additionalData
      });
    } catch (error) {
      console.error("Error creating user document", error);
    }
  }
  return getUserDocument(user.uid);
};
const getUserDocument = async uid => {
  if (!uid) return null;
  try {
    const userDocument = await firestore.doc(`users/${uid}`).get();
    return {
      uid,
      ...userDocument.data()
    };
  } catch (error) {
    console.error("Error fetching user", error);
  }
};

export const getSearchUsers = async (searchInfo) => {
  const sInfo = searchInfo.split('#');
  const disName = sInfo[0];
  const disID = parseInt(sInfo[1], 10);
  const uniqRef = firestore.collection('users');
  let snap;

  if (!isNaN(disID)){
    snap = await uniqRef.where('displayName', '==', disName).where('_id', '==', disID).get();
  }
  else {
    snap = await uniqRef.where('displayName', '==', disName).get();
  }
  
  return snap;
};

export const addChat = async (users, cuid) => {
  if (!users) return;
  await firestore.doc(`users/${users[0]}`).update({
    chats : firebase.firestore.FieldValue.arrayUnion(cuid)
  });

  await firestore.doc(`users/${users[1]}`).update({
    chats : firebase.firestore.FieldValue.arrayUnion(cuid)
  });

  return 0;
}

export const createChat = async (user, additionalData) => {
  if (!user) return;

  const usr1 = user.displayName + "#" + user._id;
  const usr2 = additionalData[1].displayName + "#" + additionalData[1]._id;
  const cuid = user.uid + ":" + additionalData[0];
  const userRef = firestore.doc(`chats/${cuid}`);
  const snapshot = await userRef.get();
  if (!snapshot.exists) {
    try {
      const users = [usr1, usr2];
      const messages = [{senders:usr1, msg:'Hello ' + usr2 + '!'},
                        {senders:usr2, msg:'Hi ' + usr1 + '!'}];

      await userRef.set({
        users,
        messages
      });

      addChat([user.uid, additionalData[0]], cuid);
    } catch (error) {
      console.error("Error creating chat", error);
    }
  }

  return cuid;
}

export const getChatInfo = async (chats, uuid) => {
  const users = [];

  for (const chat of chats) {
    const info = chat.split(':');
    let friend = info[0];
    if (friend === uuid) {
      friend = info[1];
    }
    const userRef = firestore.doc(`users/${friend}`);
    const snapshot = await userRef.get();
    if (snapshot) {
      users.push([chat, snapshot.data()]);
    };
  }
  
  return users
}