import React, { useContext, useState, useEffect} from "react";
import { UserContext } from "../providers/UserProvider";
import io from 'socket.io-client';


import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import Header from './Header';
import FriendList from './FriendList';
import Messages from './Messages';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    marginTop: 50,
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    background: '#AAAAAA'
  },
  chatBox: {
    margin: 10,
    padding: theme.spacing(1),
    color: theme.palette.text.secondary,
    minHeight: 330,
    maxHeight: 330,
    overflow: 'auto',
    
  },
  formBox: {
    margin: 10,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  textField:{
    width: '100%',
    height: '100%'
  },
  sendButton: {
    margin: 5,
    height: '80%'
  }
}));

let socket;

const ProfilePage = () => {
  const user = useContext(UserContext);
  //const {photoURL, displayName, email} = user;
  const [roomid, setRoomid] = useState("");
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);
  const [inroom, setInroom] = useState(false);
  const udisplayName = user.displayName; 
  const ENDPOINT = 'localhost:5000';
  const classes = useStyles();
  

  const friendListCallback = room_id => {
    setRoomid(room_id);
    setMessage("");
    setMessages([]);
  };

  useEffect(() => {
    if (roomid !== '') {
      socket = io(ENDPOINT);
      socket.emit('join', {udisplayName, roomid}, (error) => {
        //Error handling at join
      });

      return () => {
        socket.emit('disconnect');
        socket.off();
      }
    }
    
  }, [ENDPOINT, roomid]);

  useEffect(() => {
    if (roomid !== ''){
      socket.on('message', (message) => {
        setMessages([...messages, message]);
      });
    }
  });

  const sendMessage = (event) => {
    event.preventDefault();
    if (roomid !== '' && message){
      socket.emit('sendMessage', message, () => setMessage(''));
    }
  };
  return (
    <div className = {classes.root}>
      <Header/>
      <Grid container spacing = {3}>
        <Grid item sm={2}></Grid>
        <Grid item sm={5}>

          <Paper className={classes.paper}>
            <Paper className={classes.chatBox}>
              <Messages messages={messages} name={user.displayName}/>
            </Paper>
            <Paper className={classes.formBox}>
              <form noValidate autoComplete="off">
                <Grid container spacing = {1}>
                  <Grid item sm={9}>
                    <TextField 
                    className={classes.textField} 
                    id="messagesBox" 
                    label="Messages" 
                    variant="outlined" 
                    value = {message}
                    onChange = {(event) => setMessage(event.target.value)}/>
                  </Grid>
                  <Grid item sm={3}>
                    <Button 
                    className={classes.sendButton} 
                    type="submit" 
                    variant="contained" 
                    color="primary"
                    onClick = {sendMessage.bind(this)}>
                      Send
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </Paper>
            
          </Paper>
        </Grid>
        <Grid item sm={3}>
          <FriendList flCallback = {friendListCallback}/>
        </Grid>
        <Grid item sm={2}></Grid>
      </Grid>
    </div>
  ); 
};
export default ProfilePage;