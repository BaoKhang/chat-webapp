import React, { useContext } from "react";
import { UserContext } from "../providers/UserProvider";
import {auth} from "../config/firebase";

import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      justifyContent: 'right'
    },
    appbar: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        position: 'static',
        borderBottom: `1px solid ${theme.palette.divider}`
        
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    logo: {
        margin: 'auto',
        display: 'block',
        maxWidth: 140,
    }, 
    logoProfile: {
        flexGrow: 1,
        marginTop: 13,
        maxWidth: 140,
    },
    typo: {
        flexGrow: 1
    },
    title: {
        marginRight: 15
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
        marginRight: 20
    },
  }));


const Header = () => {
    const user = useContext(UserContext);
    const classes = useStyles();
    if (user) {
        const {_id, photoURL, displayName} = user;
        return (
            <div className={classes.root}>
                <AppBar className={classes.appbar}>
                    <Toolbar className={classes.toolbar}>
                        <img src={"https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg"} alt="logo" className={classes.logoProfile}/>
                        <Typography variant="h6" className={classes.typo}>Chat-web</Typography> 
                        <Typography variant="h6" className={classes.title}>
                            {displayName + '#' + _id}
                        </Typography>
                        <Avatar alt={displayName} src={photoURL} className={classes.large} />
                        <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        onClick = {() => {auth.signOut()}}
                        >Sign Out</Button> 
                    </Toolbar>
                </AppBar>
            </div>
            
        );
    }
    else {
        return (
            <div className={classes.root}>
                <AppBar className={classes.appbar}>
                    <Toolbar>
                        <img src={"https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg"} alt="logo" className={classes.logo}/>             
                    </Toolbar>
                </AppBar>
            </div>
            
        );
    }
    
};

export default Header;