import React, {useState} from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import SearchFriend from './SearchFriend';
import ChatList from './ChatList';

import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import List from '@material-ui/core/List';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: 50,
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      background: '#AAAAAA'
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    searchPaper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    friendPaper: {
      marginTop: 10,
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      minHeight: 300,
      maxHeight: 500,
      overflow: 'auto',
    },
    friendList: {
      width: '100%',
      maxWidth: '36ch',
      backgroundColor: theme.palette.background.paper,
    },
    tabList: {
        maxWidth: 10,
        display: 'block',
        margin: 'auto',
    },
    tabPaper: {
        marginTop: 10,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <div>{children}</div>
        )}
      </div>
    );
};

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  

function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
};

const FriendList = (props) => {
    const [searchInfo, setSearchInfo] = useState("");
    const classes = useStyles();

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
      setValue(newValue);
    };

    const onChangeHandler = event => {
      const { value } = event.currentTarget;
      setSearchInfo(value);
    };

    const chatListCallback = chatList_id => {
      props.flCallback(chatList_id);
    };

    return (
        <div className="">
            <Paper className={classes.paper}>
                <Paper className={classes.searchPaper}>
                <InputBase
                    className={classes.input}
                    placeholder="Search Users"
                    inputProps={{ 'aria-label': 'search google maps' }}
                    name='searchInfo'
                    id='searchInfo'
                    value={searchInfo}
                    onChange={e => {onChangeHandler(e)}}
                />
                
                </Paper>
                <TabPanel value={value} index={0}>
                    <Paper className={classes.friendPaper}>
                        <List className={classes.friendList}>
                            <ChatList parentCallback = {chatListCallback}/>
                        </List>
                    </Paper>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Paper className={classes.friendPaper}>
                      <List className={classes.friendList}>
                        <SearchFriend searchInfo={searchInfo}/>
                      </List>   
                    </Paper>
                </TabPanel>
                
                <Paper className={classes.tabPaper}>
                    <Tabs value={value} onChange={handleChange} aria-label="search">
                        <Tab className={classes.tabList} wrapped label="Friends" {...a11yProps(0)} />
                        <Tab className={classes.tabList} wrapped label="Search" {...a11yProps(1)} />
                    </Tabs>
                </Paper>
            </Paper>
        </div>
    );
};

export default FriendList;