import React, { useContext, useState, useEffect, Fragment} from "react";
import { UserContext } from "../providers/UserProvider";
import {getChatInfo, getSearchUsers} from "../config/firebase";

import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';


const ChatList = (props) => {
    const user = useContext(UserContext);
    const [data, setData] = useState();
    const genUsers = [];

    useEffect(() => {
        getChatInfo(user.chats, user.uid).then(result => {
            setData(result);
        });
        }, [user.chats]);
    if (data !== undefined) {
        data.forEach(args => {
            genUsers.push(args);
        });
    }

    const sendData = (roomid) => {
        props.parentCallback(roomid);
    };

    if (!genUsers.empty) {
        return (genUsers.map(friend => (
            <Fragment key={friend[1].displayName + "#" + friend[1]._id}>
                <ListItem onClick={sendData.bind(this,friend[0])}>
                    <ListItemAvatar>
                        <Avatar alt={friend[1].displayName}  src={friend[1].photoURL}/>
                    </ListItemAvatar>
                    <ListItemText
                    primary={friend[1].displayName + '#' + friend[1]._id}
                    secondary="Online"/>
                </ListItem>
                
                <Divider variant="inset" component="li" />
            </Fragment>
        )))
    }
    return (<Fragment></Fragment>);
}

export default ChatList;