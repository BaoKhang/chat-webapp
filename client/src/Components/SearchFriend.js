import React, { useContext, useState, useEffect, Fragment} from "react";
import { UserContext } from "../providers/UserProvider";
import { makeStyles } from '@material-ui/core/styles';
import {getSearchUsers, createChat} from "../config/firebase";
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';


const useStyles = makeStyles((theme) => ({
    iconButton: {
        padding: 10,
      },  
  }));

const clickEvent = (user, props) => { 
    createChat(user, props);
};

const SearchFriend = (props) => {

    const user = useContext(UserContext);

    const classes = useStyles();

    const [data, setData] = useState();

    useEffect(() => {
        // Update the document title using the browser API
        getSearchUsers(props.searchInfo).then(result => {setData(result);});
    }, [props.searchInfo]);

    const genUsers = [];
    if (data !== undefined) {
        data.docs.forEach(args => {
          genUsers.push([args.id, args.data()]);
        });
    }
    if (!genUsers.empty){
        
        return (
            
            genUsers.map((users) => (
                <Fragment key={users[0]}>
                    <ListItem>
                    <ListItemAvatar>
                        <Avatar alt={users[1].displayName}  src={users[1].photoURL}/>
                    </ListItemAvatar>
                    <ListItemText
                    primary={users[1].displayName + '#' + users[1]._id}
                    secondary="Online"/>
                    <IconButton type="submit" className={classes.iconButton} aria-label="add" onClick={clickEvent.bind(this,user, users)}>
                        <AddIcon />
                    </IconButton>

                    </ListItem>
                    
                    <Divider variant="inset" component="li" />
                </Fragment>
            ))
   
        );
    }
    else {
        return (<Fragment></Fragment>);
    }
}

export default SearchFriend;

