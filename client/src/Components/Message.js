import React from 'react';

import './Message.css';

const Message = ({message, name}) => {
    let isSentByCurrentUser = false;

    if (message.user === name) {
        isSentByCurrentUser = true;
    }

    if (isSentByCurrentUser){
        return (
            <div className="messageContainer justifyEnd">
                <p className="sentText pr-10">{name}</p>
                <div className="messageBox backgroundBlue">
                    <p className="messageText colorWhite">{message.text}</p>
                </div>
            </div> 
        )
    }
    else {
        return (
            <div className="messageContainer justifyStart">
                <div className="messageBox backgroundLight">
                    <p className="messageText colorDark">{message.text}</p>
                </div>
                <p className="sentText">{message.user}</p>
            </div> 
        )
    }
    
}

export  default Message;