const express = require('express');
const socketio = require('socket.io');
const http = require('http');
const router = require('./router');

const {addUser, removeUser, getUser, getUsersInRoom} = require('./users');

const PORT = process.env.PORT || 5000;

const app = express();
const server = http.createServer(app);
const io = socketio(server);

io.on('connection', (socket) => {
    
    socket.on('join', ({udisplayName, roomid}, callback) => {
        const {error , user} = addUser({id: socket.id, name: udisplayName, room: roomid});
        if (error) return callback(error);

        socket.emit('message', {user: 'Bot', text: `${user.name}, welcome to live-chat room`});
        socket.broadcast.to(user.room).emit('message', {user: 'Bot', text: `${user.name}, joined live-chat room`})
        socket.join(user.room);

        callback();

    });

    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id);
        if (user === undefined) return callback({error:'User undefined'});
        io.to(user.room).emit('message', {user: user.name, text: message});

        callback();
    });

    socket.on('disconnect', () => {
        const user = removeUser(socket.id);
        if (user) {
            io.to(user.room).emit('message', {user: 'admin', text: `${user.name}, has left live-chat room`});
        }
    });
});

app.use(router);

server.listen(PORT, () => console.log(`Server has started on port ${PORT}`));